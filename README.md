# vbfhcc_fit



## Getting started

The first step is to clone and compile TRExFitter and then to clone this project. The steps to do this will be something like:

```
git clone --recursive ssh://git@gitlab.cern.ch:7999/TRExStats/TRExFitter.git
#If you have problems with the above (need to configure certificate, etc.), instead replace with cp -r /afs/cern.ch/user/m/maklein/public/repositories/TRExFitter .
cd TRExFitter
source setup.sh
mkdir build
cd build
cmake ../
cmake --build ./
cd ..
git clone ssh://git@gitlab.cern.ch:7999/maklein/vbfhcc_fit.git
#If you have problems with the above (need to configure certificate, etc.), instead replace with cp -r /afs/cern.ch/user/m/maklein/public/repositories/vbfhcc_fit .
cd vbfhcc_fit
```

## Running code

To actual run the fits, two steps are needed: convert the input trees into input histograms, and then to actually run the fits.
```
root -l -q -b histor.C
source sub
```

